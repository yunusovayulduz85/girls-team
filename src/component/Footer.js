import React, { Component } from 'react';
import Logo from "../assets/Logo.svg";
import Telephone from "../assets/Telephone.svg";
import email from "../assets/pochta.svg";
import karta from "../assets/karta.svg";
export default class Footer extends Component {
    render() {
        return (
            <footer className='footer'>
                <div className='container'>
                <div className='footer__wrapper'>
                <div className='footer__wrapper--left'>
                    <div>
                        <img src={Logo} alt="Logo" />
                        <p className='finishedHous'>Отделка деревянных домов <br/>

                            в Москве и области</p>
                        <p className='INN'>ООО "Пайнвуд Хомс" <br/>
                            ИНН: 9715410587 <br/>
                            ОГРН: 1217700593857</p>
                    </div>
                    <div className='footer__menu'>
                        <div>
                            <p>Главная</p>
                            <p>Проекты</p>
                            <p>Услуги</p>
                        </div>
                        <div>
                            <p>Цены</p>
                            <p>О нас</p>
                            <p>Контакты</p>
                        </div>
                    </div>
                </div>  
                <div>
                    <p className='img__text'>
                        <img src={Telephone} alt="Telephone"/>
                            <p>+7 777 777 77 77</p>
                    </p>
                            <p className='img__text'>
                        <img src={email} alt="email"/>
                            <p>info@pinewoodhomes.ru</p>
                    </p>
                            <p className='img__text'>
                        <img src={karta} alt="karta"/>
                            <p>Владимирская обл. <br/>
                                    г. Александров<br />
                                    ул. Ленина 13/7 БЦ Свечка,<br />
                                оф. 211</p>
                    </p>
                </div>
                </div>
                </div>
            </footer>
        )
    }
}
